<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
     //Table name
     protected $table = 'groups';

     //Primary key
     public $primaryKey = 'id';
 
     //timestamps
     public $timestamps = true;
}
