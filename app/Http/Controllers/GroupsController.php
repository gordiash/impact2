<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Trainer;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy('name', 'asc')->paginate(10);

        return view('backend.groups.index')->with('groups', $groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trainers = Trainer::all();
        return view('backend.groups.create')->with('trainers', $trainers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'trainer' => 'required',

        ]);

        $day = implode(',',$request->input('day'));
        $hourArray = array_filter($request->input('hour'));
        $hour = implode(',', $hourArray);

        $group = new Group;
        $group->name = $request->input('name');
        $group->description = $request->input('description');
        $group->trainer_id = $request->input('trainer');
        $group->day = $day;
        $group->hour = $hour;
        $group->save();

        return redirect('/admin/groups')->with('success', 'Group Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::find($id);
       
        return view('backend.groups.show')->with('group', $group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trainers = Trainer::all();
        $group = Group::find($id);
        return view('backend.groups.edit')->with('group', $group)->with('trainers', $trainers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'trainer' => 'required'
        ]);

        $group = Group::find($id);
        $group->name = $request->input('name');
        $group->description = $request->input('description');
        $group->trainer_id = $request->input('trainer');
        $group->save();

        return redirect('/admin/groups')->with('success', 'Group Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);
        $group->delete();
        return redirect('/admin/groups')->with('success', 'Group Deleted');
    }
}
