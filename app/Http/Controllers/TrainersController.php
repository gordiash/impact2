<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainer;

class TrainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = Trainer::orderBy('name', 'asc')->paginate(4);

        return view('backend.trainers.index')->with('trainers', $trainers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.trainers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name'=>'required',
            'description'=>'required',
            'image'=> 'image|nullable|max:1999'
        ]);

        $trainers = new Trainer;
        $trainers->name = $request->input('name');
        $trainers->description = $request->input('description');
        
        if($request->hasFile('image')){

            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            $fileExt = $request->file('image')->getClientOriginalExtension();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $fileToStore = $fileName. '-'. time(). '.'. $fileExt;
            echo $fileToStore;
            $path = $request->file('image')->storeAs('public/uploads', $fileToStore);
        }

        $trainers->directory = $fileToStore;
        $trainers->save();

        return redirect('/admin/trainers')->with('success', 'Trainer Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trainer = Trainer::find($id);
        return view('backend.trainers.show')->with('trainer', $trainer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trainer = Trainer::find($id);
        return view('backend.trainers.edit')->with('trainer', $trainer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required',
            'description'=>'required',
            'image'=> 'image|nullable|max:1999'
        ]);

        $trainers = Trainer::find($id);
        $trainers->name = $request->input('name');
        $trainers->description = $request->input('description');
        
        if($request->hasFile('image')){

            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            $fileExt = $request->file('image')->getClientOriginalExtension();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $fileToStore = $fileName. '-'. time(). '.'. $fileExt;
            echo $fileToStore;
            $path = $request->file('image')->storeAs('public/uploads', $fileToStore);
        }else{
            $fileToStore = $trainers->directory;
        }

        $trainers->directory = $fileToStore;
        $trainers->save();

        return redirect('/admin/trainers')->with('success', 'Trainer Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trainer = Trainer::find($id);
        $trainer->delete();
        return redirect('/admin/trainers')->with('success', 'Trainer Deleted');
    }
}
