<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;


class NewsController extends Controller
{
    public function index()
    {

        $news = News::orderBy('created_at', 'desc')->paginate(5);

        return view('backend.news.index')->with('news', $news);
    }

    public function create()
    {
        return view('backend.news.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $news = new News;
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        $news->save();

        return redirect('/admin/news')->with('success','Post Created');
    }

    public function show($id)
    {
        $news = News::find($id);
        return view('backend.news.show')->with('news', $news);
    }
    public function edit($id)
    {
        $news = News::find($id);
        return view('backend.news.edit')->with('news', $news);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $news = News::find($id);
        $news->title = $request->input('title');
        $news->content = $request->input('content');
        $news->save();


        return redirect('/admin/news')->with('success', 'Post Updated');
    }

    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect('/admin/news')->with('success', 'Post Deleted');
    }
}
