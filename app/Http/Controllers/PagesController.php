<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\About;
use App\Trainer;
use App\Group;

class PagesController extends Controller
{
    public function index(){
       $news = News::orderBy('created_at', 'desc')->take(3)->get();
        return view('frontend.pages.index')->with('news', $news);
    }

    public function about(){
        $about = About::all()->first();
        $trainers = Trainer::all();
   
        return view('frontend.pages.about')->with('about', $about)
                    ->with('trainers', $trainers);
    }

    public function groups(){
        $trainers = Trainer::all();
        $groups = Group::all();
        

        return view('frontend.pages.groups')
        ->with('groups', $groups)
        ->with('trainers', $trainers);
    }

   

    public function contact(){
        return view('frontend.pages.contact');
    }
}
