<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
   //Table name
   protected $table = 'abouts';

   //Primary key
   public $primaryKey = 'id';

   //timestamps
   public $timestamps = true;

}
