<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
   //Table name
   protected $table = 'trainers';

   //Primary key
   public $primaryKey = 'id';

   //timestamps
   public $timestamps = true;
}
