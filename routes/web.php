<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// website routes
use Illuminate\Support\Facades\Request;
use App\Mail\ContactMail;


Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/groups', 'PagesController@groups');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', function(Request $request){

    Mail::send(new ContactMail($request));

    return redirect('/');
});

//admin routes
Route::prefix('admin')->group(function(){
Route::resource('about', 'AboutController');
Route::resource('news', 'NewsController');
Route::resource('groups', 'GroupsController');
Route::resource('trainers', 'TrainersController');
Route::get('/', 'BackendPagesController@admin');
});



Auth::routes();

Route::get('admin', 'HomeController@index');
