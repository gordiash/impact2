@extends('layouts.app')

@section('content')


<section>
    <div class="row justify-content-center mt-5">
            <h3>Write to Us:</h3>
    </div>

    @include('inc.contactForm')


    <div class="map row justify-content-center my-5">
        <h3 class="col-md-12 text-center align-self-center">You can find Us at:</h3>
        <div id="map" style="width: 400px; height: 400px" class="col-md-5"></div>

    </div>

    <div class="row text-center justify-content-center my-5">

        <address class="address col-auto font-weight-bold">
            Impact-Boxing and Fitness<br>
            Unit B<br> Tenlons Road Industrial Estate,<br>
            Tenlons Road,<br>
            Nuneaton<br>
        </address>


    </div>

</section>

@endsection
