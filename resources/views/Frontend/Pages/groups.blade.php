@extends('layouts.app')

@section('content')

<section class="parallax-2 ">
    <div class="overlay text-center">
        <h2>
            Groups
        </h2>
    </div>
</section>

<section class="Groups mt-5">
    <div class="row justify-content-center ">
        <form action="{{action('PagesController@about')}}" method="post" class="text-center col-md-12">
            <div class="row justify-content-center">
                <div class="col-sm-4 col-md-2 align-self-center ml-3">
                    <label for="classes">Select group:</label>
                </div>
                <div class="col-sm-7 col-md-3">
                    <select class="form-control" name="class" id="groups">
                        <option value="" selected disabled>Choose your group</option>

                        @foreach ($groups as $group)
                        <option onclick="show({{$group->id}})" value="{{$group->id}}">{{$group->name}}</option>
                        @endforeach


                    </select>
                </div>


            </div>
            {{-- //<input type="submit" name="submit" value="Submit" class="btn btn-info mt-3"> --}}
        </form>
    </div>


    <div class="row justify-content-center">
        @foreach ($groups as $group)
        @foreach ($trainers as $trainer)


        <div id="{{$group->id}}" class="col-md-5 jumbotron group mt-5" style="display:none">
            <div class="text-center">
                <h3>{{$group->name}}</h3>
                @if ($trainer->id ==$group->trainer_id)
                <p>Trainer: {{$trainer->name}}</p>
                @endif
            </div>
            <div class="card-body">
                <p>{{$group->description}}</p>

            </div>
            <div class="text-center">
                <div>
                    <p>Training sessions are on:</p>
                    <div id="days" data-days="{{$group->day}}"> </div>

                    <div id="hour" data-hour="{{$group->hour}}">
                        
                    </div>
                </div>


            </div>
        </div>
        @endforeach
        @endforeach
    </div>







</section>

@endsection
