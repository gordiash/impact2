@extends('layouts.app')

@section('content')

<div class="top-context row">
    <div class=" overlay text-center">

        <h1 class="main-font">Impact Boxing</h1>
        <h2 class="main-font">and Fitness</h2>
    </div>
</div>

<div class="header-break my-4">
    <div class="header-break__text blockquote">
        <q class="header-break__quote text-center mr-2 ml-2">He who is not courageous enough to take risks will
            accomplish nothing
            in life</q>
        <p class="header-break__author mr-3 mt-4 ">- Muhammad Ali</p>
    </div>
</div>

<!-- Paralax break -->
<div class="parallax-1  mb-4">
    <div class="parallax-1__text overlay text-center">
        <h2 style="font-weight:bold">Latests News:</h2>
    </div>
</div>


<!-- Main section -->
<div id='main'>


    <div class="row justify-content-center">

        @foreach ($news as $item)

        <div class="card col-md-6 mx-3 my-3">
            <div class="card-header">
                <h4 class="card-title">{{$item->title}}</h4>

            </div>
            <div class="card-body">
                <div class="container">
                    <p class="card-text">{{$item->content}}</p>
                </div>
                

            </div>
            <div class="card-footer">
                <small class="text-muted">Created at: {{$item->created_at}}</small>
                <a href="#" class="float-right "><i class="fab fa-facebook fa-2x right"></i></a>
                <a href="#" class="float-right mr-2"><i class="fab fa-twitter-square fa-2x right"></i></a>
            </div>
        </div>



        @endforeach




    </div>
</div>

<!-- Paralax break -->
<div class="parallax-2 ">
    <div class="parallax-2__text overlay text-center">
        <h2 style="font-weight:bold">They said about Us:</h2>
    </div>
</div>

@include('inc.slider')








@endsection
