@extends('layouts.app')

@section('content')

<!-- Paralax break -->
<section class="parallax-1 ">
    <div class="overlay text-center">
        <h2>Who we are?</h2>
    </div>
</section>

<!-- Main section -->
<Section class="about-text text-center">

    <p>{{$about->description}}</p>

</Section>

<!-- Paralax break -->
<div class="parallax-2 ">
    <div class="parallax-2__text overlay text-center">
        <h2 style="font-weight:bold">Our Team:</h2>
    </div>
</div>

<section class="team mt-4">


    <div class="row justify-content-center">

        @foreach ($trainers as $trainer)

        <div class="card col-md-7 my-5">
            <div class="team-member">
                <div class="card-header row ">
                    <img class="col-md-3 rounded " src="{{asset('storage/uploads/'.$trainer->directory)}}" alt="placeholder">
                    <h2 class="card-title col-md-7 align-self-center">{{$trainer->name}}</h2>
                </div>
                <div class="card-body ">
                    <q>{{$trainer->description}}</q>
                </div>
            </div>
        </div>


    
    

    @endforeach

</div> {{-- end row --}}



</section>


@endsection
