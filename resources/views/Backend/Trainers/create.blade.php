@extends('layouts.adminDashboard')

@section('admin-content')

<div class="container">
    <form action="{{action('TrainersController@store')}}" method="POST" class="row" enctype="multipart/form-data">
        @csrf
        <div class="form-row justify-content-center">
            <div class="col-md-8 form-group row mt-5 justify-content-start">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name">
            </div>
            <div class="col-md-10 form-group row my-5 justify-content-start">
                <label for="description">Description:</label>
                <textarea name="description" id="description" cols="30" rows=""></textarea>
            </div>

            <div class="col-md-10 form-group row my-5">
                <label for="image">Add Image:</label>
                <input type="file" name="image" id="image">
            </div>

            <div class="col-md-6 justify-content-start row">
                <input type="submit" value="Submit" class="btn btn-info ">
            </div>
        </div>




    </form>
</div>





@endsection
