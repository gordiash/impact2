@extends('layouts.adminDashboard')


@section('admin-content')

<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-7 my-3">
                    <a class="btn btn-success" href="/admin/trainers/create">Add Trainer</a>
                </div>

                <div class="col-md-7">
                        {{$trainers->links()}}
                    </div>

        @if (count($trainers)>0)
        @foreach ($trainers as $trainer)
        <div class="card col-md-7 my-3 mr-3">
            <div class="card-header">
                <div class="row">
                    <img class="col-md-3" src="{{asset('storage/uploads/'.$trainer->directory)}}" alt="" srcset="">
                <div class="card-title col-md-3 align-self-center"><a href="/admin/trainers/{{$trainer->id}}">{{$trainer->name}}</a> </div>
                </div>
            
            </div>
            <div class="card-body">
                {{$trainer->description}}
            </div>
            <div class="card-footer">
                <p class="text-muted">Created at: {{$trainer->created_at}}</p>
            </div>
            <div class="row col-md-12">

                <div class="col-md-3 justify-content-start row">
                    <a class="btn btn-success my-3" href="/admin/trainers/{{$trainer->id}}/edit">Edit</a>
                </div>

                <div class="col-md-9 justify-content-end row">
                    <form action="{{action('TrainersController@destroy', $trainer->id)}}" method="POST">
                        @method("DELETE")
                        @csrf
                        <input type="submit" value="Delete" class="btn btn-danger my-3">
                    </form>
                </div>
            </div>
        </div>


        @endforeach
        

        @else

        @endif

       

    </div>
</div>






@endsection
