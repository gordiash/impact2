@extends('layouts.adminDashboard')

@section('admin-content')

<div class="container">


    <div class="row justify-content-center">
        <div class="col-md-6  row mt-5">
        <img class="col-md-3" src="{{asset('/storage/uploads/'.$trainer->directory)}}" alt="" srcset="">
            <h2 class="align-self-center">{{$trainer->name}}</h2>
        </div>
        <div class="col-md-7  row my-5">
            <p>{{$trainer->description}}</p>
        </div>
        <div class="col-md-7  row my-5">
                <p class="text-muted">Created at: {{$trainer->created_at}}</p>
            </div>

        <div class="col-md-6 justify-content-center row">
            <a class="btn btn-success" href="/admin/trainers/{{$trainer->id}}/edit">Edit</a>
        </div>
        
        <div class="col-md-6 justify-content-center row">
            <form action="{{action('TrainersController@destroy', $trainer->id)}}" method="POST">
                @method("DELETE")
                @csrf
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>


</div>





@endsection
