@extends('layouts.adminDashboard')

@section('admin-content')

<div class="container">
    <form action="{{action('AboutController@update', $about->first()->id) }}" method="POST" class="row">
@csrf
@method('PUT')

        <div class="form-row justify-content-center">
           
            <div class="col-md-12 form-group row my-5">
                <label for="description">Content:</label>
            <textarea name="description" id="description" cols='50' >{{$about->first()->description}}</textarea>
            </div>

            <div class="col-md-6 justify-content-center row">
    <input type="submit" value="Submit" class="btn btn-info ">
    </div>
        </div>

        


    </form>
</div>





@endsection
