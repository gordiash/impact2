@extends('layouts.adminDashboard')


@section('admin-content')

<div class="container">
    <div class="row mt-5">
    <p class="col-md-12 jumbotron">{{$about->first()->description}}</p>



    <div class="col-md-3 my-5">
        <a class="btn btn-info" href="/admin/about/create">Write About</a>
    </div>
    <div class="col-md-3 my-5">
        <a class="btn btn-success" href="/admin/about/{{$about->first()->id}}/edit">Edit About</a>
    </div>


    </div>



</div>


@endsection
