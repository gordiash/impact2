@extends('layouts.adminDashboard')


@section('admin-content')

<div class="container">
    <div class="row ">
        <form action="{{action('GroupsController@store')}}" method="post" class="row mt-5 col-md-12 ">

            @csrf

            <div class="row justify-content-center">
                <div class="row col-md-8 justify-content-center">
                    <label class="col-md-8" for="name">Group Name:</label>
                    <input class="col-md-8" type="text" id="name" name="name">
                </div>

                <div class="row col-md-8 justify-content-center my-3">
                    <label class="col-md-8" for="description">Description:</label>
                    <textarea class="col-md-8" name="description" id="description"></textarea>
                </div>

                <div class="row col-md-8 justify-content-center my-3">
                    <label class="col-md-8 " for="trainer">Choose Trainer:</label>
                    <select class="col-md-8" name="trainer" id="trainer">

                        <option value="" disabled selected>Choose trainer</option>

                        @foreach ($trainers as $trainer)
                        <option value="{{$trainer->id}}">{{$trainer->name}}</option>
                        @endforeach


                    </select>
                </div>

                <div class="row col-md-8 mt-5 justify-content-center">
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Monday"><span class="ml-2 mr-5">Monday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Tuesday"><span class="ml-2 mr-5">Tuesday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Wednesday"><span class="ml-2 mr-5">Wednesday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Thursday"><span class="ml-2 mr-5">Thursday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Friday"><span class="ml-2 mr-5">Friday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Saturday"><span class="ml-2 mr-5">Saturday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" name="day[]" id="" value="Sunday"><span class="ml-2 mr-5">Sunday</span>
                        <label for="hour">Set time:</label>
                        <input type="time" name="hour[]" id="hour" min="0:00" max="23:00" >
                    </div>

                </div>

                <div class="col-md-12 row mt-5 justify-content-center">
                    <input type="submit" value="Submit" class="btn btn-info ">
                </div>

            </div>
        </form>

    </div>
</div>






@endsection
