@extends('layouts.adminDashboard')


@section('admin-content')

<div class="container">
    <div class="row ">
        <form action="{{action('GroupsController@update', $group->id)}}" method="post" class="row mt-5 col-md-12 ">

            @csrf
            @method('PUT')

            <div class="row justify-content-center">
                <div class="row col-md-8 justify-content-center">
                    <label class="col-md-8" for="name">Group Name:</label>
                <input class="col-md-8" type="text" id="name" name="name" value="{{$group->name}}">
                </div>

                <div class="row col-md-8 justify-content-center my-3">
                    <label class="col-md-8" for="description">Description:</label>
                <textarea class="col-md-8" name="description" id="description">{{$group->description}}</textarea>
                </div>

                <div class="row col-md-8 justify-content-center my-3">
                    <label class="col-md-8 " for="trainer">Choose Trainer:</label>
                    <select class="col-md-8" name="trainer" id="trainer">

                        <option value="" disabled selected>Choose trainer</option>

                        @foreach ($trainers as $trainer)
                        <option value="{{$trainer->id}}">{{$trainer->name}}</option>
                        @endforeach


                    </select>
                </div>

                <div class="col-md-12 row mt-5 justify-content-center">
                    <input type="submit" value="Submit" class="btn btn-info">
                </div>

            </div>
        </form>

    </div>
</div>






@endsection
