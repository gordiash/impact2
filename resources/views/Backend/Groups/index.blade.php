@extends('layouts.adminDashboard')


@section('admin-content')

<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-7 my-3">
            <a class="btn btn-success" href="/admin/groups/create">Add group</a>
        </div>

        <div class="col-md-7">
            {{$groups->links()}}
        </div>


        @if (count($groups)>0)
        @foreach ($groups as $group)
        <div class="card col-md-7 my-3 mr-3">
            <div class="card-header">
                <div class="card-title"><a href="/admin/groups/{{$group->id}}">{{$group->name}}</a> </div>
            </div>
            <div class="card-body">
                {{$group->description}}
            </div>
            <div class="card-footer">
                <p class="text-muted">Created at: {{$group->created_at}}</p>
            </div>
            <div class="row col-md-12">

                <div class="col-md-3 justify-content-start row">
                    <a class="btn btn-success my-3" href="/admin/groups/{{$group->id}}/edit">Edit</a>
                </div>

                <div class="col-md-9 justify-content-end row">
                    <form action="{{action('GroupsController@destroy', $group->id)}}" method="POST">
                        @method("DELETE")
                        @csrf
                        <input type="submit" value="Delete" class="btn btn-danger my-3">
                    </form>
                </div>
            </div>
        </div>


        @endforeach

        @endif



    </div>
</div>






@endsection
