@extends('layouts.adminDashboard')

@section('admin-content')

<div class="container">
    <form action="{{action('NewsController@store')}}" method="POST" class="row">
@csrf
        <div class="form-row justify-content-center">
            <div class="col-md-6 form-group row mt-5">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title">
            </div>
            <div class="col-md-7 form-group row my-5">
                <label for="content">Content:</label>
                <textarea name="content" id="content" cols="30" rows="10"></textarea>
            </div>

            <div class="col-md-6 justify-content-center row">
    <input type="submit" value="Submit" class="btn btn-info ">
    </div>
        </div>

        


    </form>
</div>





@endsection
