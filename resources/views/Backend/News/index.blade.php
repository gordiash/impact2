@extends('layouts.adminDashboard')


@section('admin-content')

<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-7 my-3">
                    <a class="btn btn-success" href="/admin/news/create">Add News</a>
                </div>

                <div class="col-md-7">
                        {{$news->links()}}
                    </div>

        @if (count($news)>0)
        @foreach ($news as $item)
        <div class="card col-md-7 my-3 mr-3">
            <div class="card-header">
                <div class="card-title"><a href="/admin/news/{{$item->id}}">{{$item->title}}</a> </div>
            </div>
            <div class="card-body">
                {{$item->content}}
            </div>
            <div class="card-footer">
                <p class="text-muted">Created at: {{$item->created_at}}</p>
            </div>
            <div class="row col-md-12">

                <div class="col-md-3 justify-content-start row">
                    <a class="btn btn-success my-3" href="/admin/news/{{$item->id}}/edit">Edit</a>
                </div>

                <div class="col-md-9 justify-content-end row">
                    <form action="{{action('NewsController@destroy', $item->id)}}" method="POST">
                        @method("DELETE")
                        @csrf
                        <input type="submit" value="Delete" class="btn btn-danger my-3">
                    </form>
                </div>
            </div>
        </div>


        @endforeach
        

        @else

        @endif

        

    </div>
</div>






@endsection
