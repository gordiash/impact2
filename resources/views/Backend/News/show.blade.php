@extends('layouts.adminDashboard')

@section('admin-content')

<div class="container">


    <div class="row justify-content-center">
        <div class="col-md-6  row mt-5">

            <h2>{{$news->title}}</h2>
        </div>
        <div class="col-md-7  row my-5">
            <p>{{$news->content}}</p>
        </div>
        <div class="col-md-7  row my-5">
                <p class="text-muted">Created at: {{$news->created_at}}</p>
            </div>

        <div class="col-md-6 justify-content-center row">
            <a class="btn btn-success" href="/admin/news/{{$news->id}}/edit">Edit</a>
        </div>
        
        <div class="col-md-6 justify-content-center row">
            <form action="{{action('NewsController@destroy', $news->id)}}" method="POST">
                @method("DELETE")
                @csrf
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>


</div>





@endsection
