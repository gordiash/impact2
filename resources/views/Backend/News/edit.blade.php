@extends('layouts.adminDashboard')

@section('admin-content')

<div class="container">
    <form action="{{action('NewsController@update', $news->id)}}" method="post" class="row">
        @csrf
        @method("PUT")
        <div class="form-row col-md-12 justify-content-center">
            <div class="col-md-7 form-group row mt-5 ml-4">
                <label for="title">Title:</label>
                <input class="col-md-7" type="text" name="title" id="title" value="{{$news->title}}">
            </div>
            <div class="col-md-7 form-group row my-5">
                <label for="content">Content:</label>
                <textarea name="content" id="content" class="col-md-7">{{$news->content}}</textarea>
            </div>

            <div class="col-md-6 justify-content-center row">
                <input type="submit" value="Submit" class="btn btn-info ">
            </div>
        </div>




    </form>
</div>





@endsection
