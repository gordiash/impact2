<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Impact Boxing and Fitness') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/appImpact.js')}}" defer></script>

    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:700|Roboto+Mono&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/v4-shims.css">


</head>

<body>
    <div id="app">
        <nav class="navbar navbar-dark sticky-top bg-dark navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand main-font" href="{{ url('/') }}">
                    {{ config('app.name', 'Impact Boxing and Fitness') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto text-white text-center">
                        <!-- Authentication Links -->
                        @guest
                        {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif --}}
                        <li class="nav-item mr-2"><a href="/about" class="nav-link ">About</a></li>
                        <li class="nav-item mr-2"><a href="/groups" class="nav-link">Groups</a></li>
                        <li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="container-fluid">
            @yield('content')
        </main>
    </div>


    <footer class="footer mt-5">

        <div class="footer-copyright text-center">

            &copy <?php echo Date('Y')?> Impact Boxing and Fitness.<br> All Rights Reserved.

        </div>


        <div class="social-icons text-center my-3">
            <i class="fab fa-facebook fa-2x"></i>
            <i class="fab fa-instagram fa-2x"></i>
            <i class="fab fa-twitter-square fa-2x"></i>
        </div>

    </footer>

{{-- Vue JS --}}
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- FontAwesome scripts -->
    <script defer src="https://use.fontawesome.com/releases/v5.10.0/js/all.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.10.0/js/v4-shims.js"></script>
    <!-- Google MAP API -->
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxD_IqEm-_Fx7BuVfm3UbboUcLlS9joGw&callback=initMap"
        type="text/javascript"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>




</body>

</html>
