<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Impact Boxing and Fitness') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/appImpact.js')}}" defer></script>

    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:700|Roboto+Mono&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/dashboard-custom.css') }}" rel="stylesheet">
{{-- <link href="{{ asset('css/custom.css') }}" rel="stylesheet"> --}}
   

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/v4-shims.css">


</head>

<body>
<div class="container-fluid">

    <div class="row">
        <nav class="navbar navbar-dark bg-dark p-0 shadow col-md-12">
            <a class="navbar-brand col-sm-3 col-md-2 mr-0"
                href="/admin">{{ config('app.name', 'Impact Boxing and Fitness') }}</a>

            
                @auth
    

    
                <li class="nav-item dropdown  mr-5" style="list-style:none">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            
                @endauth
          
        </nav>



        <nav id="admin-sidenav" class="col-md-2 d-none d-md-block bg-light sidebar bg-dark">
            <div class="sidebar-sticky">
                <ul class="nav flex-column align-content-center ">
                    <li class="nav-item my-3 ">
                        <a class="nav-link ml-3" href="/admin">
                                <i class="fas fa-tachometer-alt"></i>
                            Dashboard 
                        </a>
                    </li>
                    <li class="nav-item my-3">
                        <a class="nav-link ml-3" href="/admin/news">
                            <i class="far fa-newspaper"></i>
                            News
                        </a>
                    </li>
                    <li class="nav-item my-3">
                        <a class="nav-link ml-3" href="/admin/groups">
                            <i class="fas fa-users"></i>
                            Groups
                        </a>
                    </li>
                    <li class="nav-item my-3">
                    <a class="nav-link ml-3" href="/admin/about">
                            <i class="fas fa-edit"></i>
                            About
                        </a>
                    </li>
                    <li class="nav-item my-3">
                        <a class="nav-link ml-3" href="/admin/trainers">
                            <i class="fas fa-user"></i>
                            Trainers
                        </a>
                    </li>
                   
                </ul>
            </div>

        </nav>

        <main class="col-md-9">
            @include('inc.messages')
            @yield('admin-content')
        </main>

    </div>
</div>







    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script>
        window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')

    </script>
    <script src="/docs/4.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous">
    </script>



</body>

</html>
