<section class="slide-show">


    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner mt-5">
            <div class="carousel-item active text-center ">
                <div  class="row d-flex h-100 align-items-center justify-content-center slide-item">
                    <q class="blockquote col-sm-12">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deserunt nemo,
                        amet saepe, at obcaecati quia ex repellat nostrum officia laboriosam veniam, corrupti harum
                        aperiam nam?
                        Vero et atque error!</q>
                    <footer class="blockquote-footer text-right mr-5 align-self-end mb-5">John</footer>
                </div>

            </div>

            <div class="carousel-item text-center ">
                <div class="row d-flex h-100 align-items-center justify-content-center slide-item">
                    <q class="blockquote col-sm-12">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deserunt nemo,
                        amet saepe, at obcaecati quia ex repellat nostrum officia laboriosam veniam, corrupti harum
                        aperiam nam?
                        Vero et atque error!</q>
                    <footer class="blockquote-footer text-right mr-5 align-self-end mb-5">Vikki</footer>
                </div>
            </div>

            <div class="carousel-item text-center ">
                <div class="row d-flex h-100 align-items-center justify-content-center slide-item">
                    <q class="blockquote col-sm-12">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam deserunt nemo,
                        amet saepe, at obcaecati quia ex repellat nostrum officia laboriosam veniam, corrupti harum
                        aperiam nam?
                        Vero et atque error!</q>
                    <footer class="blockquote-footer text-right mr-5 align-self-end mb-5">Bob</footer>
                </div>
            </div>
        </div>
    </div>



</section>
