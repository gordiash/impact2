<form action="{{url('/contact')}}" method="post" autocomplete="on" class="form-group mt-5 text-center">
        <div class="row text-center form-contact">

            <div class="row col-md-12 justify-content-center">
                <div class="col-md-1">
                    <i class="far fa-user-circle fa-2x"></i>
                </div>
                <div class="col-2 sr-only">
                    <label for="first_name">First Name</label>
                </div>
                <div class="col-md-4">
                    <input id="first_name" name="firstName" type="text" class="form-control" placeholder="First Name">
                </div>

            </div>
            <div class="row mt-3 col-md-12 justify-content-center">
                <div class="col-md-1">
                    <i class="far fa-user-circle fa-2x"></i>
                </div>
                <div class="col-2 sr-only">
                    <label for="last_name">Last Name</label>
                </div>
                <div class="col-md-4">
                    <input id="last_name" name="lastName" type="text" class="form-control" placeholder="Last Name">
                </div>
            </div>

            <div class="row mt-3 col-md-12 justify-content-center">
                <div class="col-md-1">
                    <i class="fas fa-at fa-2x"></i>
                </div>
                <div class="col-2 sr-only">
                    <label for="email">Email</label>
                </div>
                <div class="col-md-4">
                    <input id="email" name="email" type="text" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="row mt-3 col-md-12 justify-content-center">
                <div class="col-md-1 ml-1">
                    <i class="fas fa-mobile-alt fa-2x"></i>
                </div>
                <div class="col-2 sr-only">
                    <label for="mobile">Mobile</label>
                </div>
                <div class="col-md-4 ml-1">
                    <input id="mobile" name="mobile" type="text" class="form-control" placeholder="Mobile">
                </div>
            </div>

            <div class="row mt-3 col-12 justify-content-center">
                    <div class="col-md-1 ml-1">
                            <i class="far fa-comment-alt fa-2x"></i>
                        </div>
                <div class="col-md-4">
                    <textarea id="textarea1" name="content" class="form-control" placeholder="Type your message..."></textarea>
                </div>
            </div>

        </div>

        <button type="submit" class="btn btn-info mt-3">Submit</button>

    </form>